# Robotelephant

Command-line tool to automatically detect and post a #nowplaying toot on Mastodon.

## Installation

You'll need Python 3 and the dependencies from the `requirements.txt` file. Then start the script and follow the instructions from the setup wizard.